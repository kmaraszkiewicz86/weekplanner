﻿using System.ComponentModel;
using Xamarin.Forms;
using WeekPlanner.ViewModels;

namespace WeekPlanner.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}
