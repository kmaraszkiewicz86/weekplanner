﻿using System;
using System.Collections.Generic;
using WeekPlanner.ViewModels;
using WeekPlanner.Views;
using Xamarin.Forms;

namespace WeekPlanner
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}

